﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMissile : MonoBehaviour
{
    public GameObject missilePrefab;
    public float respawnTime = 7.5f;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine es como un update, pero en vez de ejecutarlo cada frame, lo ejecuta cada x tiempo, segun el tiempo que le has retornado.
        InvokeRepeating("spawnthis", 7.5f, 4f);
    }
    private void Update()
    {
        if (Playermovement.dead)
        {
            CancelInvoke();
        }
    }

    // Update is called once per frame
    private void spawnthis()
    {
        GameObject missilegenerated = Instantiate(missilePrefab);
        //9.5 es la x del collider de la camara para generar terreno.
        missilegenerated.transform.position = new Vector2(transform.position.x + 9.5f, Random.Range(-3.5f, 3.5f));
        //missilegenerated.transform.Rotate(0,0,360);
        //missilegenerated.transform.localScale= new Vector2(2, 2);
    }
}
