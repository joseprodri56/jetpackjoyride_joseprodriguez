﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{
    public static bool dead = false;
    //"y"'s velocity max speed
    public int limit = 7;
    public static int speed = 5;
    public float fasterTime = 7.5f;
    public GameObject lobby;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("starting", 0f);
    }

    // Update is called once per frame
    void Update()
    {

        speedup();
        move();
        //print("Y speed " + this.GetComponent<Rigidbody2D>().velocity.y);
        dontExceed();
        //Descomenta para mirar la posicion de la x del jugador
        //print(this.transform.position.x);
        //print(this.GetComponent<Rigidbody2D>().velocity.x);


    }

    void starting()
    {
        GameObject startingTheGame = Instantiate(lobby);
        startingTheGame.transform.position = new Vector2(this.transform.position.x + 6, lobby.transform.position.y);
    }
    private void dontExceed()
    {
        
        if (this.GetComponent<Rigidbody2D>().velocity.y > limit)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y -1);
        }
    }

    //Nota: fijate en poner 2D en el collisionenter2D, y los collision usan collision2d, los triggers collider2d.
    private void OnCollisionStay2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "Kill")
        {
            Destroy(gameObject);
            dead = true;
        }
        //Esto hace reparar un bug en el que la velocity "y" se me volvia loco al tocar al suelo.
        if(collision.gameObject.tag == "GroundPreFap")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Kill")
        {
            Destroy(gameObject);
            dead = true;
        }
    }
    //public float JPgravity = 0.1;
    private void move()
    {
        if (Input.GetKey("space"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y+1);
            
        }
    }

    private void speedup()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y);
        fasterTime -= Time.deltaTime;
        if (fasterTime <= 0)
        {
            speed++;
            fasterTime = 7.5f;
            print("speed incremented to " + speed);
        }
    }
}
