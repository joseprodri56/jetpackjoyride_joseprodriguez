﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    private Text gameOverText;
    void Start()
    {
        gameOverText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Playermovement.dead == true)
        {
            gameOverText.text = "GAME OVER. Score " + Score.scoreInt;
        }
    }
}
