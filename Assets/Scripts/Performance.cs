﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Performance : MonoBehaviour
{
    private Transform cameraPosition;
    // Start is called before the first frame update
    void Start()
    {
        cameraPosition = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 temp = transform.position;
        temp.x = cameraPosition.position.x-30;
        //Nota: esto es para seguir la y. Lo descomento porque la y de la camara nunca se mueve
        //temp.y = cameraPosition.position.y;
        transform.position = temp;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Destruye todo lo que toca.
        Destroy(collision.gameObject);
        
    }
}
