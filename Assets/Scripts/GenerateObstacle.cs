﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObstacle : MonoBehaviour
{
    public GameObject obstaclePrefab;
    public float respawnTime = 3f;
    // Start is called before the first frame update
    void Start()
    {
        //Courotine es como el invokerepeating, pero para indicar el tiempo tienes que hacer un return, y almenos te deja editar el tiempo.
        StartCoroutine(generating());
        
    }
    private void Update()
    {
        if (Playermovement.speed >= 10)
        {
            respawnTime = 2f;
        }
        if(Playermovement.speed >= 15)
        {
            respawnTime = 1.5f;
        }
    }

    // Update is called once per frame
    private void spawnthis()
    {
        GameObject obstaclegenerated = Instantiate(obstaclePrefab);
        //9.5 es la x del collider de la camara para generar terreno.
       obstaclegenerated.transform.position = new Vector2(transform.position.x + 9.5f, Random.Range(-3.5f, 3.5f));
        obstaclegenerated.transform.Rotate(0,0, Random.Range(0, 360));
        obstaclegenerated.transform.localScale= new Vector2(1, Random.Range(3, 5));
    }
    IEnumerator generating()
    {
        
        while (!Playermovement.dead)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnthis();
        }
    }
}
