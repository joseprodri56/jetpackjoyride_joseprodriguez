﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    //Es un metodo casero. Medire el score segun su x.
    private Transform playerx;
    public static int scoreInt;
    private Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();
        playerx = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
        //Sumo 6 porque el score es la posicion que esta la x. Y como empieza con -6 (tema posicion del prefab del terreno). Le sumo
        scoreInt = (int) playerx.position.x+6;
        scoreText.text = "Score: "+ scoreInt;
        }
        catch (Exception)
        {
            Destroy(gameObject);
        }
        

    }
}
